﻿using System;

namespace Download.Lib
{
   internal class Range
   {
      public long Start { get; set; }
      public long End { get; set; }
   }
}