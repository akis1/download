﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;

namespace Download.Lib
{
    public static class Downloader
    {
        static Downloader()
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.DefaultConnectionLimit = 100;
            ServicePointManager.MaxServicePointIdleTime = 1000;
        }

        public static DownloadResult Download(string fileUrl, string destinationFolderPath,
            int numberOfParallelDownloads = 0, bool validateSSl = false)
        {
            if (!validateSSl)
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var uri = new Uri(fileUrl);
            //destination path 
            string destinationFilePath = Path.Combine(destinationFolderPath, uri.Segments.Last());

            DownloadResult result = new DownloadResult() {FilePath = destinationFilePath};

            //number of parallel downloads
            if (numberOfParallelDownloads <= 0)
                numberOfParallelDownloads = Environment.ProcessorCount;

            #region filesize

            WebRequest webRequest = WebRequest.Create(fileUrl);
            webRequest.Method = "HEAD";
            long responseLength;
            using WebResponse webResponse = webRequest.GetResponse();
            responseLength = long.Parse(webResponse.Headers.Get("Content-Length"));
            result.Size = responseLength;
            using FileStream destinationStream = new FileStream(destinationFilePath, FileMode.Append);
            ConcurrentDictionary<int, string> tempFileDictionary = new ConcurrentDictionary<int, string>();
            List<Range> readRanges = new List<Range>();

            for (int chunk = 0; chunk < numberOfParallelDownloads - 1; chunk++)
            {
                var range = new Range()
                {
                    Start = chunk * (responseLength / numberOfParallelDownloads),
                    End = ((chunk + 1) * (responseLength / numberOfParallelDownloads)) - 1
                };
                readRanges.Add(range);
            }

            readRanges.Add(new Range()
            {
                Start = readRanges.Any() ? readRanges.Last().End + 1 : 0,
                End = responseLength - 1
            });

            #endregion

            DateTime starTime = DateTime.Now;

            #region parallel downloads

            int index = 0;
            Parallel.ForEach(readRanges, new ParallelOptions() {MaxDegreeOfParallelism = numberOfParallelDownloads},
                readRange =>
                {
                    HttpWebRequest httpWebRequest = HttpWebRequest.Create(fileUrl) as HttpWebRequest;
                    httpWebRequest.Method = "GET";
                    httpWebRequest.AddRange(readRange.Start, readRange.End);
                    using (HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse)
                    {
                        string tempFilePath = Path.GetTempFileName();
                        using (var fileStream =
                            new FileStream(tempFilePath, FileMode.Create, FileAccess.Write, FileShare.Write))

                        {
                            httpWebResponse.GetResponseStream().CopyTo(fileStream);
                            tempFileDictionary.TryAdd((int) index, tempFilePath);
                        }
                    }

                    index++;
                });
            result.ParallelDownloads = index;

            #endregion

            result.TimeTaken = DateTime.Now.Subtract(starTime);

            #region merge to single file

            foreach (var tempFile in tempFileDictionary.OrderBy(b => b.Key))
            {
                byte[] tempFileBytes = File.ReadAllBytes(tempFile.Value);
                destinationStream.Write(tempFileBytes, 0, tempFileBytes.Length);
                File.Delete(tempFile.Value);
            }

            #endregion

            return result;
        }
    }
}