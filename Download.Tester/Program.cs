﻿using System;
using Download.Lib;

namespace Download.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Downloader.Download("", @"c:\temp\", 2);

            Console.WriteLine($"Location: {result.FilePath}");
            Console.WriteLine($"Size: {result.Size}bytes");
            Console.WriteLine($"Time taken: {result.TimeTaken.Milliseconds}ms");
            Console.WriteLine($"Parallel: {result.ParallelDownloads}");

            Console.ReadKey();
        }
    }
}